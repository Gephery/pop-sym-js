(() => {
function crSep(text){
  let zero = '0'.charCodeAt(0);
  let nine = '9'.charCodeAt(0);
  let ni = 0; 
  while (text.charCodeAt(ni) < zero || text.charCodeAt(ni) > nine) {
    ni = ni + 1;
  }
  let col = text.substr(0, ni);
  let row = text.substr(ni, text.length);
  return [col, parseInt(row)];
}

// thanks https://stackoverflow.com/questions/1431094/how-do-i-replace-a-character-at-a-particular-index-in-javascript
String.prototype.replaceAt=function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
}

var doc;
var ctx;
var myChart;
var canvas;
var rm;
var workbook;
var table; // Datatable 
var zoomNum = 0;

//var backgroundColor = [
//  'rgba(255, 99, 132, 0.2)',
//  'rgba(54, 162, 235, 0.2)',
//  'rgba(255, 206, 86, 0.2)',
//  'rgba(75, 192, 192, 0.2)',
//  'rgba(153, 102, 255, 0.2)',
//];
//var borderColor = [
//  'rgba(255, 99, 132, 1)',
//  'rgba(54, 162, 235, 1)',
//  'rgba(255, 206, 86, 1)',
//  'rgba(75, 192, 192, 1)',
//  'rgba(153, 102, 255, 1)',
//];

var eventMonMeta = {
  name: 'Event Monitoring',
  nFields: 4,
  vendorCR: 'D2',
  indexCR: 'E2',
  fieldsCR: 'F1',
  dataCR: 'F2',
  nData: 4
};

var storageMeta = {
  name: 'Storage',
  nFields: 6,
  vendorCR: 'D12',
  indexCR: 'E12',
  fieldsCR: 'F11',
  dataCR: 'F12',
  nData: 4
};

var siemMeta = {
  name: 'SIEM / Log Tool',
  nFields: 8,
  vendorCR: 'A23',
  indexCR: 'B23',
  fieldsCR: 'D22',
  dataCR: 'D23',
  nData: 4
};

var analyticsToolMeta = {
  name: 'Analytics Tool',
  nFields: 11,
  vendorCR: 'A23',
  indexCR: 'B23',
  fieldsCR: 'L22',
  dataCR: 'L23',
  nData: 4
}

var siemAndLogIntMeta = {
  name: 'SIEM/Log Integration',
  nFields: 1,
  vendorCR: 'A23',
  indexCR: 'B23',
  fieldsCR: 'C22',
  dataCR: 'C23',
  nData: 4,
  compound: [siemMeta, analyticsToolMeta]

};

var metaSets = [eventMonMeta, storageMeta, siemAndLogIntMeta];
var zoomMetaSets = [];
var zoomLevel = 0;
var zoomStack = [];

function loadXLSX(callback) {
  let reader = new FileReader();
  reader.readAsArrayBuffer(doc.files[0]);
  reader.onload = () => {
    let data = new Uint8Array(reader.result);
    workbook = XLSX.read(data, {type: 'array'});
    callback();
  };
}

function setChartType2(title) {
  myChart.type = 'line';
  myChart.options =  {
    //onClick: zoomOutOnClick,
    title: {
      text: `${title} (Lv${zoomLevel})`,
      display: true
    },
    legend:  {
    display: false,
    position: 'left'
    },
    responsive: false,
    tooltips:  {
      mode: 'point'
    },
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero: true
          }
      }]
    }
  };
}

function setChartType1() {
  myChart.type = 'line';
  myChart.options =  {
    //onClick: zoomInOnClick,
    title: {
      text: 'Overall',
      display: true
    },
    legendCallback: legendCustom,
    legend:  {
      display: false,
    //position: 'left'
    },
    responsive: false,
    tooltips:  {
      mode: 'point'
    },
    scales: {
      yAxes: [{
          ticks: {
              beginAtZero: true
          }
      }]
    }
  };
}

function createChart() {
  if (myChart == undefined) 
    return new Chart(ctx, {
      type: 'line',
      options: {
          responsive: false,
      }
    });
  else 
    return myChart;
}

// Excepts is Set
function hideAllData(excepts) {
  for (let i = 0; i < myChart.data.datasets.length; i++) {
    if (i < zoomNum || myChart.data.datasets[i].label == '' || (excepts != undefined && 
        excepts.has(myChart.data.datasets[i].label)))
      myChart.data.datasets[i].hidden = false;
    else 
      myChart.data.datasets[i].hidden = true;
  }
}

function showAllData() {
  for (let i = 0; i < myChart.data.datasets.length; i++) {
    myChart.data.datasets[i].hidden = false;
  }
  myChart.update();
}

function excelNext(t, ci, ri) {
  let info = crSep(t);
  let column = info[0];
  let i = column.length - 1;
  while (ci != 0) {
    if (i == -1) {
      column = 'A' + column;
      ci = ci - 1;
      i = column.length;
    } else if (column.charAt(i) != 'Z') {
      column = column.replaceAt(i, String.fromCharCode(column.charCodeAt(i) + 1));
      ci = ci - 1;
      i = column.length - 1;
    } else {
      column = column.replaceAt(i, 'A');
      i = i - 1;
    }
  }
  let row = info[1] + ri;
  return column + row;
}

function getColor(vendorI) {
  let o = vendorI % 3;
  let c0 = vendorI < 6 ? vendorI * 50 : (vendorI % 6) * 50;
  let c1 = vendorI < 36 ? Math.floor(vendorI / 6) * 50 : Math.floor(vendorI % 36) * 50;
  let c2 = o == 0 ? 150 : (o == 1 ? 200 : 250);
  let ca = o == 0 ? c0 : (o == 1 ? c1 : c2);
  let cb = o == 0 ? c1 : (o == 1 ? c2 : c0);
  let cc = o == 0 ? c2 : (o == 1 ? c0 : c1);
  let color = `rgba(${ca}, ${cb}, ${cc}, `;
  return color;
}

function setT1DataSet() {
  let work = workbook.Sheets.Sheet1;
  let labels = [];
  let ds = [];
  let i = 0;
  let count = 0; // Initial index means always 1 data
  zoomNum = metaSets.length;
  metaSets.forEach((s) => {
    // Labels
    //labels.push(' '); // Index area to match up datasets, not needed as meta name
    labels.push(s.name);
    let extraS = (s.compound == undefined) ? 0 : s.compound.length;
    for (let ii = 0; ii < s.nFields + extraS - 1; ii++) {
      labels.push('');
    }

    // Stripping & overall datasets
    let data = new Array(count).fill(null);
    data = data.concat(new Array(s.nFields + extraS + 1).fill(6));
    count = count + s.nFields + extraS; // +1 for each dataset a new index column
    ds.push({
      label: s.name,
      data: data,
      backgroundColor: i % 2 == 0 ? 'rgba(0,0,0,0.1)' : 'rgba(0,0,0,0)',
      borderWidth: 0,
      pointRadius: 0,
      fill: true,
      tension: 0
    });
    i = i + 1;
  });

  let dss = [];
  metaSets.forEach((s) => {
    let li = s.vendorCR;
    let ii = s.indexCR;
    let di = s.dataCR;
    // Add data points
    for (i = 0; i < s.nData; i = i+1) {
      let data = [];
      let vendorI = parseInt(work[ii].v) - 1;
      //data.push(work[ii].v); // Index to continue line
      for (let j = 0; j < s.nFields; j = j+1) {
        data.push(work[excelNext(di, j, 0)].v);
      }
      // Add compound sums after 
      if (s.compound != undefined) {
        for (let c = 0; c < s.compound.length; c++) {
          let sum = 0;
          let innerComp = s.compound[c];
          let compDat = innerComp.dataCR;
          for (let cc = 0; cc < innerComp.nFields; cc++) {
            sum += work[excelNext(compDat, cc, i)].v;
          }
          data.push(sum / innerComp.nFields);
        }
      }
      if (dss[vendorI] == undefined) {
          let color = getColor(vendorI);
          dss[vendorI] = {
          label: work[li].v,
          data: data,
          borderColor: color + '1)',
          backgroundColor: color + '0.5)',
          borderWidth: 2,
          fill: false, // are under
          tension: 0 // straight line
        };
      } else {
        dss[vendorI].data = dss[vendorI].data.concat(data);
      }
      li = excelNext(li, 0, 1);
      ii = excelNext(ii, 0, 1);
      di = excelNext(di, 0, 1);
    }
  });
  ds = ds.concat(dss);
  myChart.data.labels = labels;
  myChart.data.datasets = ds;
}

function setT2DataSet(meta) {
  currentMeta = meta;
  let work = workbook.Sheets.Sheet1;
  let labels = [];
  let ds = [];
  let i = 0;
  // Labels
  for (let ii = 0; ii < meta.nFields; ii++) {
    labels.push(work[excelNext(meta.fieldsCR, ii, 0)].v);
  }

  // Add comp labels
  let count = meta.nFields;
  if (meta.compound != undefined) {
    // Setup zoomout meta 
    zoomNum = meta.compound.length;
    zoomMetaSets = Array.from(meta.compound);
    zoomMetaSets.unshift(0);
    zoomMetaSets.push(meta.parent);
    
    // Add compound clickables
    for (let c = 0; c < meta.compound.length; c++) {
      let comp = meta.compound[c];
      // Name 
      labels.push(comp.name);

      // Skip labels 
      let extraS = (comp.compound == undefined) ? 0 : comp.compound.length;
      for (let ii = 0; ii < comp.nFields + extraS - 1; ii++) {
        labels.push('');
      }
      // Striping of compounds
      let data = new Array(count).fill(null);
      data = data.concat(new Array(comp.nFields + extraS + 1).fill(6));
      count = count + comp.nFields + extraS; // +1 for each dataset a new index column
      ds.push({
        label: comp.name,
        data: data,
        backgroundColor: i % 2 == 0 ? 'rgba(0,0,0,0.1)' : 'rgba(0,0,0,0)',
        borderWidth: 0,
        pointRadius: 0,
        fill: true,
        tension: 0
      });
      i++;
    }
  } else {
    zoomNum = 0;
    zoomMetaSets = [];
  }

  let li = meta.vendorCR;
  let ii = meta.indexCR;
  let di = meta.dataCR;
  for (i = 0; i < meta.nData; i = i+1) {
    data = [];
    let vendorI = parseInt(work[ii].v) - 1;
    for (let j = 0; j < meta.nFields; j = j+1) {
      data.push(work[excelNext(di, j, 0)].v);
    }
    // Add compounds after
    if (meta.compound != undefined) {
      for (let c = 0; c < meta.compound.length; c++) {
        let innerComp = meta.compound[c];
        let compDat = innerComp.dataCR;
        for (let cc = 0; cc < innerComp.nFields; cc++) {
          data.push(work[excelNext(compDat, cc, i)].v);
        }
      }
    }
    let color = getColor(vendorI);
    ds.push({
      label: work[li].v,
      data: data,
      borderColor: color + '1)',
      backgroundColor: color + '0.5)',
      borderWidth: 2,
      fill: false, // are under
      tension: 0 // straight line
    });
    li = excelNext(li, 0, 1);
    ii = excelNext(ii, 0, 1);
    di = excelNext(di, 0, 1);
  }
  ds.push({
    label: '',
    data: [6],
    backgroundColor: 'rgba(0,0,0,0)',
    borderWidth: 0,
    pointRadius: 0,
    fill: false,
    tension: 0
  });
  myChart.data.labels = labels;
  myChart.data.datasets = ds;
}

function changeDataset() {
  let sel = table.rows({selected: true}).data();
  let sel2 = new Set();
  for (let i = 0; i < sel.length; i++) {
    sel2.add(sel[i][1]);
  }
  hideAllData(sel2);

  myChart.update();
}

function _loadChart() {
  myChart = createChart();
  setChartType1();
  setT1DataSet();
  myChart.update();
  let tableQ = $('#table-id');
  tableQ.html(legendCustom());
  table = tableQ.DataTable({
    columnDefs: [ {
      orderable: false,
      className: 'select-checkbox',
      targets:   0
    } ],
    select: {
      style:    'os',
      selector: 'td:first-child'
    },
    order: [[ 1, 'asc' ]]
  });
  table.on('select', changeDataset);
  table.on('deselect', changeDataset);
  hideAllData();
}

function loadChart(e) {
  loadXLSX(_loadChart);
}

function isInChart(e) {
  let b = myChart.chart.chartArea;
  let box = canvas.getBoundingClientRect();
  let top = b.top + box.y;
  let bottom = b.bottom + box.y;
  let left = b.left + box.x;
  let right = b.right + box.x;
  let x = e.clientX;
  let y = e.clientY;
  return x >= left && x <= right && y >= top && y <= bottom;
}

function zoomInOnClick(e) {
  let dI = myChart.getElementsAtXAxis(e)[0];
  if (dI != undefined && isInChart(e)) {
    dI = dI._datasetIndex;
    let meta = zoomLevel == 0 ? metaSets : zoomMetaSets;
    if (meta[0] == 0) dI++;
    zoomLevel++;
    setChartType2(meta[dI].name);
    setT2DataSet(meta[dI]);
    zoomStack.push(meta[dI]);
    myChart.update();
    document.getElementById('my-chart').onclick = zoomOutOnClick;
    changeDataset();
  }
}

function zoomOut(e) {
  let dI = e == undefined ? undefined : myChart.getElementsAtXAxis(e)[0];
  if (dI != undefined && zoomLevel != 0 && zoomMetaSets.length > (dI._datasetIndex + 2)) {
    zoomInOnClick(e);
    //zoomLevel = zoomLevel == 0 ? 0 : zoomLevel - 1;
    return;
  } else if (zoomLevel > 1) {
    zoomStack.pop();
    let meta = zoomStack.pop()
    zoomStack.push(meta);
    zoomLevel = zoomLevel == 0 ? 0 : zoomLevel - 1;
    setChartType2(meta.name);
    setT2DataSet(meta);
  } else {
    zoomStack = [];
    zoomLevel = 0;
    setChartType1();
    setT1DataSet();
  }

  changeDataset();
  if(zoomStack.length == 0) document.getElementById('my-chart').onclick = zoomInOnClick;
  else document.getElementById('my-chart').onclick = zoomOutOnClick;
}

function zoomOutOnClick(e) {
  if (isInChart(e)) {
    zoomOut(e);
  }
}

function legendCustom(chart) {
  let work = workbook.Sheets.Sheet1;

  // Generate col titles
  let titles = [];
  titles.push('<thead><tr>');
  titles.push('<th></th>');
  titles.push('<th>Names</th>');
  titles.push('</tr></thead>');
  titles = titles.join('');

  // Generate rows
  let comp = metaSets[0];
  let rows = [];
  rows.push('<tbody>');
  for (let i = 0; i < comp.nData; i++) {
    rows.push(`<tr><td></td><td>${work[excelNext(comp.vendorCR, 0, i)].v}</td></tr>`);
  }
  rows.push('</tbody>');
  rows = rows.join('');

  // Done
  return titles + rows;
}

window.onload = function() {
  // assign 
  doc = document.getElementById('file-in');
  sAll = document.getElementById('s-all');
  ctx = document.getElementById('my-chart').getContext('2d');
  canvas = document.getElementById('my-chart');

  // Handlers
  doc.addEventListener('change', loadChart);
  sAll.addEventListener('click', showAllData);
  canvas.onclick = zoomInOnClick;
  canvas.addEventListener('contextmenu', function(e) {
    e.preventDefault();
    zoomOut();
  }, false);
}

})();
